#!/bin/bash
trap "cleanup" INT
function cleanup() {
	echo -ne "\n"
	exit 0
}
if [ -z $1 ]; then
	read -p "Client name: " name
else
	 name=$1
fi
export EASYRSA_BATCH=1
export EASYRSA_REQ_CN=$name
mkdir ~/client-configs
cd ~/easy-rsa
printf "\n== Generating request for $name...\n"
./easyrsa gen-req $name nopass
printf "\n== Signing $name...\n" 
cp pki/private/$name.key ~/client-configs/keys/
./easyrsa sign-req client $name
printf "\n== Copying $name certificate in ~/client-configs...\n"
cp pki/issued/$name.crt ~/client-configs/keys/
sudo cp /etc/openvpn/server/ca.crt ~/client-configs/keys/
sudo chown $USER:$USER ~/client-configs/keys/*
printf "\n== Generating $name.ovpn...\n"
KEY_DIR=~/client-configs/keys
OUTPUT_DIR=~/client-configs/files
BASE_CONFIG=~/client-configs/base.conf
cat ${BASE_CONFIG} \
    <(echo -e '<ca>') \
    ${KEY_DIR}/ca.crt \
    <(echo -e '</ca>\n<cert>') \
    ${KEY_DIR}/${name}.crt \
    <(echo -e '</cert>\n<key>') \
    ${KEY_DIR}/${name}.key \
    <(echo -e '</key>\n<tls-crypt>') \
    ${KEY_DIR}/${name}.key \
    <(echo -e '</tls-crypt>') \
    > ${OUTPUT_DIR}/${name}.ovpn

sed -i "s/; script-security 2/script-security 2/g" ${OUTPUT_DIR}/${name}.ovpn
sed -i "s/; up \/etc\/openvpn\/update-resolv-conf/up \/etc\/openvpn\/update-resolv-conf/g" ${OUTPUT_DIR}/${name}.ovpn
sed -i "s/; down \/etc\/openvpn\/update-resolv-conf/down \/etc\/openvpn\/update-resolv-conf/g" ${OUTPUT_DIR}/${name}.ovpn

printf "\n== done!\n"

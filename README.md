# == GARAGENUMVPN - LE VPN DU GARAGE ==

# INSTALLATION  

-   Executer le script `server.sh` pour installer le VPN sur la machine serveur  

-   Executer le script `sign-clients.sh` pour ajouter des clients  

# UTILISATION

<details>
<summary>Windows</summary>

## Installation

Téléchargez l'application client OpenVPN pour Windows depuis la [page de téléchargement d'OpenVPN](https://openvpn.net/index.php/open-source/downloads.html). Choisissez la version d'installation appropriée pour votre version de Windows.

!!! note "Remarque"  
    OpenVPN a besoin de privilèges administratifs pour s'installer.

Après avoir installé OpenVPN, copiez le fichier .ovpn dans :

`C:\Program Files\OpenVPN\config`

Lorsque vous lancez OpenVPN, il localisera automatiquement le profil et le rendra disponible.

Vous devez **exécuter OpenVPN en tant qu'administrateur** à chaque fois qu'il est utilisé, même par des comptes administratifs. Pour effectuer cette action sans avoir à cliquer sur le bouton droit de la souris et à sélectionner Exécuter en tant qu'administrateur chaque fois que vous utilisez le VPN, vous devez le pré-régler à partir d'un compte administratif. Cela signifie également que les utilisateurs standard devront entrer le mot de passe de l'administrateur pour utiliser OpenVPN. Les utilisateurs standard ne peuvent pas se connecter correctement au serveur à moins que l'application OpenVPN sur le client n'ait des droits d'administrateur, les privilèges élevés sont donc nécessaires.

Pour que l'application OpenVPN s'exécute toujours en tant qu'administrateur, cliquez avec le bouton droit de la souris sur son icône de raccourci et allez dans **Propriétés**. En bas de l'onglet **Compatibilité**, cliquez sur le bouton **Modifier les paramètres pour tous les utilisateurs**. Dans la nouvelle fenêtre, cochez **Exécuter ce programme en tant qu'administrateur**.

## Connexion

À chaque fois que vous lancez l'interface graphique d'OpenVPN, Windows vous demande si vous souhaitez autoriser le programme à apporter des modifications à votre ordinateur. Cliquez sur **Oui**. Le lancement de l'application client OpenVPN ne fait que placer l'applet dans la barre d'état système afin que vous puissiez connecter et déconnecter le VPN selon vos besoins : il n'établit pas réellement la connexion VPN.

Une fois qu'OpenVPN est lancé, initiez une connexion en vous rendant dans l'applet de la barre d'état système et en cliquant avec le bouton droit de la souris sur l'icône de l'applet OpenVPN. Cela ouvre le menu contextuel. Sélectionnez client1 en haut du menu (c'est votre profil **client1**.ovpn) et choisissez **Connecter**.

Une fenêtre d'état s'ouvrira, montrant la sortie du journal pendant que la connexion est établie, et un message s'affichera une fois que le client sera connecté.

Déconnectez-vous du VPN de la même manière : allez dans l'applet de la barre d'état système, cliquez avec le bouton droit de la souris sur l'icône de l'applet OpenVPN, sélectionnez le profil du client et cliquez sur **Déconnecter**.
</details>
<details>
<summary>Linux</summary>

## Installation

Si vous utilisez Linux, vous pouvez utiliser plusieurs outils en fonction de votre distribution. Votre environnement de bureau ou votre gestionnaire de fenêtres peut également inclure des utilitaires de connexion.

La façon la plus universelle de se connecter, cependant, est d'utiliser simplement le logiciel OpenVPN.

Sur Ubuntu ou Debian, vous pouvez l'installer comme vous l'avez fait sur le serveur en tapant :

    sudo apt update
    sudo apt install openvpn

**Configuration des clients qui utilisent systemd-resolved**  
Vérifiez d'abord si votre système utilise systemd-resolved pour traiter la résolution DNS en vérifiant le fichier /etc/resolv.conf :

    cat /etc/resolv.conf

---
    Output
    # This file is managed by man:systemd-resolved(8). Do not edit.
    . . .
    nameserver 127.0.0.53
    options edns0

Si votre système est configuré pour utiliser systemd-resolved pour la résolution DNS, l'adresse IP après l'option nameserver sera 127.0.0.53. Il devrait également y avoir des commentaires dans le fichier, comme le résultat qui est montré, qui expliquent comment systemd-resolved gère le fichier. Si vous avez une adresse IP différente de 127.0.0.53, il y a de fortes chances que votre système n'utilise pas systemd-resolved et vous pouvez passer à la section suivante sur la configuration des clients Linux qui ont un script update-resolv-conf.

Pour prendre ces clients en chage, installez d'abord le package openvpn-systemd-resolved Il fournit des scripts qui forceront systemd-resolved à utiliser le serveur VPN pour la résolution DNS.

    sudo apt install openvpn-systemd-resolved

Une fois le package installé, configurez le client pour l'utiliser et pour envoyer toutes les requêtes DNS sur l'interface VPN. Ouvrez le fichier VPN du client :

    nano client1.ovpn

Décommentez maintenant les lignes suivantes que vous avez ajoutées précédemment :

    client1.ovpn
---
    script-security 2
    up /etc/openvpn/update-systemd-resolved
    down /etc/openvpn/update-systemd-resolved
    down-pre
    dhcp-option DOMAIN-ROUTE .

**Configuration des clients qui utilisent update-resolv-conf​​​**  
Si votre système n'utilise pas systemd-resolved pour gérer les DNS, vérifiez si votre distribution comprend un script /etc/openvpn/update-resolv-conf :

    ls /etc/openvpn

---
    Output
    update-resolv-conf

Si votre client inclut le fichier update-resolv-conf, alors modifiez le fichier de configuration du client OpenVPN que vous avez transféré précédemment :

    nano client1.ovpn

Décommentez les trois lignes que vous avez ajoutées pour ajuster les paramètres DNS :
    
    client1.ovpn
---
    script-security 2
    up /etc/openvpn/update-resolv-conf
    down /etc/openvpn/update-resolv-conf

Enregistrez et fermez le fichier.

## Connexion

Maintenant, vous pouvez vous connecter au VPN en pointant simplement la commande openvpn sur le fichier de configuration client :

    sudo openvpn --config client1.ovpn

Cela devrait vous connecter à votre VPN.

Remarque : si votre client utilise systemd-resolved pour gérer les DNS, vérifiez que les paramètres sont appliqués correctement en exécutant systemd-resolve --statt comme ceci :

    systemd-resolve --status tun0

Vous devriez voir une sortie similaire à la suivante :

    Output
    Link 22 (tun0)
    . . .
         DNS Servers: 208.67.222.222
                      208.67.220.220
          DNS Domain: ~.

Si vous voyez les adresses IP des serveurs DNS que vous avez configurés sur le serveur OpenVPN, avec le paramètre ~. pour DNS Domain​​ dans la sortie, alors vous avez correctement configuré votre client pour utiliser le résolveur DNS du serveur VPN. Vous pouvez également vérifier que vous envoyez des requêtes DNS sur le VPN en utilisant un site comme [DNS leak test.com](https://www.dnsleaktest.com/).

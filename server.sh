#!/bin/bash
CN=garage
if [[ $EUID == 0 ]]; then
   printf "Must be launch with user permissions. Exiting." 
   exit 1
fi
trap "cleanup" INT
function cleanup() {
	printf "\n== Cleaning...\n"
	rm -rf ~/easy-rsa
	sudo rm -rf /etc/openvpn/server/*
	rm -rf ~/client-configs
	sudo sed -i "s/net.ipv4.ip_forward = 1//g" /etc/sysctl.conf
	exit 0
}
export EASYRSA_BATCH=1
#############
## EASYRSA ##
#############
sudo apt -qq update 2>/dev/null | printf "\n== Running apt update...\n"
sudo apt -yqq install easy-rsa openvpn 2>/dev/null | printf "\n== Installing easy-rsa & openvpn...\n"
mkdir ~/easy-rsa
ln -s /usr/share/easy-rsa/* ~/easy-rsa/
chmod 700 /home/$USER/easy-rsa
cd ~/easy-rsa
printf "\n== Creating a public key infrastructure (pki)...\n"
./easyrsa init-pki
printf "\n== Creating a certificate autority (ca)...\n"
cat << EOF > vars
set_var EASYRSA_REQ_CN         "$CN"
set_var EASYRSA_REQ_COUNTRY    "FR"
set_var EASYRSA_REQ_PROVINCE   "Paris"
set_var EASYRSA_REQ_CITY       "Paris"
set_var EASYRSA_REQ_ORG        "Le Garage Numerique"
set_var EASYRSA_REQ_EMAIL      "admin@example.com"
set_var EASYRSA_REQ_OU         "Community"
set_var EASYRSA_ALGO           "ec"
set_var EASYRSA_DIGEST         "sha512"
set_var EASYRSA_CA_EXPIRE      "3650"
set_var EASYRSA_CERT_EXPIRE    "3650"
EOF
cat vars
printf "\n== Building the certificate autority...\n"
./easyrsa build-ca nopass
printf "\n== Importing the certificate autority...\n"
sudo cp pki/ca.crt /usr/local/share/ca-certificates
sudo update-ca-certificates
printf "\n== Creating server\'s private key and certificate request...\n"
./easyrsa --batch gen-req $CN nopass
printf "\n== Copying server\'s private key to OpenVPN directory...\n"
sudo cp pki/private/$CN.key /etc/openvpn/server/
printf "\n== Signing certificate request with CA...\n"
./easyrsa sign-req server $CN
printf "\n== Copying issued files into OpenVPN directory...\n"
sudo cp pki/issued/$CN.crt pki/ca.crt /etc/openvpn/server/
printf "\n== Generating tls-crypt pre-shared key...\n"
openvpn --genkey --secret ta.key
sudo cp ta.key /etc/openvpn/server/
printf "\n== Preparing client certificate and key pair...\n"
mkdir -p ~/client-configs/keys
chmod -R 700 ~/client-configs
#############
## OPENVPN ##
#############
port=1194
public_ip=$(curl -s ifconfig.me)

printf "\n== Configuring OpenVPN...\n"
conf=/etc/openvpn/server/server.conf
sudo cp /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz /etc/openvpn/server/
sudo gunzip /etc/openvpn/server/server.conf.gz
sudo bash -c "cat << EOF >> $conf 

auth SHA256
user nobody
group nogroup
EOF"

sudo sed -i "s/dh dh2048.pem/;dh dh2048\ndh none/g" $conf
sudo sed -i "s/cert server.crt/cert $CN.crt/g" $conf
sudo sed -i "s/key server.key/key $CN.key/g" $conf
sudo sed -i "s/proto udp/proto tcp/g" $conf
sudo sed -i "s/port 1194/port $port/g" $conf
sudo sed -i "s/explicit-exit-notify 1/explicit-exit-notify 0/g" $conf
sudo bash -c 'cat << EOF >> /etc/sysctl.conf

net.ipv4.ip_forward = 1
EOF'
sudo sysctl -p

printf "\n== Configuring firewall (ufw)...\n"
sudo ufw allow $port/udp
sudo ufw allow OpenSSH
sudo ufw disable
printf "y\n\n" | sudo ufw enable > /dev/null 2>&1
printf 'Firewall is active and enabled on system startup'
printf "\n== Starting OpenVPN...\n"
sudo systemctl -f enable openvpn-server@server.service
sudo systemctl start openvpn-server@server.service

printf "\n== Creating client config infrastructure...\n"
clientconf=~/client-configs/base.conf
mkdir ~/client-configs/files

cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf ~/client-configs/base.conf
sed -i "s/remote my-server-1 1194/remote $publicip $port/g" $clientconf
sed -i "s/;user nobody/user nobody/g"  $clientconf
sed -i "s/;group nogroup/group nogroup/g" $clientconf
sed -i "s/ca ca.crt/;ca ca.crt/g" $clientconf
sed -i "s/cert client.crt/;cert client.crt/g" $clientconf
sed -i "s/key client.key/;key client.key/g" $clientconf
sed -i "s/tls-auth ta.key 1/;tls-auth ta.key/g" $clientconf
cat << EOF >> $clientconf
cipher AES-256-GCM
auth SHA256
key-direction 1
; script-security 2
; up /etc/openvpn/update-resolv-conf
; down /etc/openvpn/update-resolv-conf
EOF
printf "\n== done!\n"
